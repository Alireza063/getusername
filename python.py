from functions import getUsernameByPhoto , saveInFile , cv2 
import glob


def is_integer(n):
    try:
        float(n)
    except ValueError:
        return False
    else:
        return float(n).is_integer()
    
i = 0
for photo in glob.glob("images/*.jpg"):

    result = getUsernameByPhoto(photo)
    
    if result[0] is  None:
        cv2.imwrite("mask/Fail_" + photo.split('/' , 1)[-1], result[1])
        print(photo + " :: FAIL")
        continue

    # cv2.imshow('Crop Image', result[4])
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    # break
    cv2.imwrite("mask/" + result[0][0] + ".jpg", result[1])

    if is_integer(result[0][0] ):
        cv2.imwrite(result[0][0]  + ".jpg", result[1])
        # cv2.imwrite(result[0] + ".jpg", result[2])
    else:
        if not i:
            saveInFile(photo + "::  " + result[0][0] + "\n" , 'usernames.txt' , 1)
        else:
            saveInFile(photo + "::  " + result[0][0] + "\n" , 'usernames.txt' , 0)

        print(photo , "::  " , result[0][0] )
    
    i+=1


