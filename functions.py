from PIL import Image
import numpy as np  
import re , glob , pytesseract , cv2

def getUsernameByPhoto(nameFile , croped = 1):
    image = cv2.imread(nameFile, 1)

    x = 0
    y = 30
    # width  = int(image.shape[1] / 4 * 3)
    width  = image.shape[1]
    height = int(image.shape[0] / 12 * 2 )

    if croped:
        image = image[y:y+height, x:x+width]

    hsv  = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    low  = np.array([0, 0, 235])
    up   = np.array([200, 100, 255])
    mask = cv2.inRange(hsv, low, up)


    data = pytesseract.image_to_string(mask)


    matches = re.search("([A-Za-z](?:(?:[A-Za-z0-9_]|(?:\.(?!\.)))(?:[A-Za-z0-9_]))?){7,28}", data)
    
    return [matches ,  mask , image , data , hsv] 




def saveInFile(data , name = 'test.txt' , rewrite = 1):
    if(rewrite):
        metod = 'w'
    else:
        metod = 'a'
    
    file = open(name, metod)
    file.write(data)
    file.close()


